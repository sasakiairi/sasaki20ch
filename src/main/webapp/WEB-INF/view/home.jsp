<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<title>ホーム画面</title>
</head>
<body>
<script type="text/javascript">
function checkMessage(){
	for(var i =0; i<(document.form.length); i++){
	  const comment = document.form[i].comment.value;
	  if(!comment.match(/\$/g) || comment == null ){
	    window.alert('投稿内容を入力してください');
	    return false;
	  }else if (comment.length > 500){
	  window.alert('500文字以内で入力してください');
	  return false;
	  }
	}return true;
}
</script>

	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="errorMessage">
					<li><c:out value="${errorMessage}" />
				</c:forEach>
			</ul>
			<c:remove var="errorMessages" scope="session" />
		</div>
	</c:if>
<div class="header">
	<a href="/20ch/message/">新規投稿</a>
	<a href="/20ch/login/">管理者ログイン</a>
</div>
	<h1>20ch投稿一覧</h1>
	<ol>
	<c:forEach items="${messages}" var="message">
		<c:if test="${message.deleteId == 0 }">
			<form:form modelAttribute="messageForm" id="messageForm" action="/20ch/deleteMessage/" method="post">
				<li>
					<c:out value="${message.message}"></c:out>
					<c:out value="${message.createdDate}"/>
						<form:input type="hidden" path="deleteId" value="1"/>
						<form:input type="hidden" path="id" value="${message.id}"/>
						<input type="submit" value="削除" ><br>
			</form:form>
						<c:forEach items="${comments}" var="comment">
							<c:if test="${comment.messageId == message.id }">&nbsp;&nbsp;
								<form:form modelAttribute="commentForm" id="commentForm" action="/20ch/deleteComment/" method="post">
									<c:if test="${comment.deleteId == 0 }">
									<c:out value="${comment.comment}"/>
									<c:out value="${comment.createdDate}"/>
										<c:if test="${ not empty password }">
										<form:form modelAttribute="messageForm" id="messageForm" action="/20ch/login/" method="post">


											<form:input type="hidden" path="deleteId" value="1"/>
											<form:input type="hidden" path="id" value="${comment.id}"/>
											<input type="submit" value="コメント削除" ><br>

										</form:form>
										</c:if>
									</c:if>
									<c:if test="${comment.deleteId == 1 }">
										<form:form modelAttribute="messageForm" id="messageForm" action="/20ch/login/" method="post">
										<c:if test="${ not empty password }">
											このコメントは管理者によって削除されました。
											<form:input type="hidden" path="deleteId" value="0"/>
											<form:input type="hidden" path="id" value="${comment.id}"/>
											<input type="submit" value="コメント復活" ><br>
										</c:if>
										</form:form>
									</c:if>
								</form:form>
							</c:if>
						</c:forEach>
			<form:form modelAttribute="commentForm" name="form" onSubmit="return checkMessage()">
				<form:textarea type="text" path="comment" cols="50" rows="5"/>
				<input type="hidden" name="messageId" value="${message.id}">
				<input type="submit" value="コメント投稿" ><br>
			</form:form><br>
		</c:if>
		<c:if test="${message.deleteId == 1 }">
			<form:form modelAttribute="messageForm" id="messageForm" action="/20ch/deleteMessage/" method="post">
				<li>この投稿は管理者によって削除されました。
				<form:input type="hidden" path="deleteId" value="0"/>
				<form:input type="hidden" path="id" value="${message.id}"/>
				<input type="submit" value="復活" >
			</form:form>
		</c:if>


	</c:forEach>
	</ol>
</body>
</html>