<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta charset="utf-8">
<title>新規投稿画面</title>
</head>
<body>
<script type="text/javascript">
	function checkMessage(){
	  const message = document.form.message.value;
	  if(!message.match(/\$/g) || message == null ){
	    window.alert('投稿内容を入力してください');
	    return false;
	  }else if (message.length > 1000){
		  window.alert('1000文字以内で入力してください');
		  return false;
	  } else {
		  return true;
	  }
	}
</script>
	<div class="header">
		<a href="/20ch/home/">トップ</a>
	</div>
	<h1>新規投稿入力画面</h1>
	<form:form modelAttribute="messageForm" name="form" onSubmit="return checkMessage()">
		<form:textarea path="message" cols="100" rows="10"/>
		<input type="submit" value="投稿">
	</form:form>
</body>
</html>
