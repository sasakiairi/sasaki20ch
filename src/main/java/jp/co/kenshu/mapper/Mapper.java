package jp.co.kenshu.mapper;


import java.sql.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import jp.co.kenshu.entity.Comment;
import jp.co.kenshu.entity.Message;

public interface Mapper {
	Message getMessage(int id);
	List<Message> getMessageAll();
	List<Comment> getCommentAll();
	int insertMessage(String message);
	Date createdDate();
	Comment getComment(int id);
	int updateComment(int messageId);
	int insertComment(@Param("comment") String comment, @Param("message_id") int messageId, @Param("created_date") Date createdDate, @Param("updated_date") Date updatedDate);
	int deleteMessage(@Param("deleteId") int deleteId, @Param("id") int id);
	int deleteComment(@Param("deleteId") int deleteId, @Param("id") int id);

}
