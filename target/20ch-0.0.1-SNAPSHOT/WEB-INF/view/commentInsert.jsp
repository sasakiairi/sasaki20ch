<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta charset="utf-8">
<title>コメント入力画面</title>
</head>
<body>

	<div class="header">
		<a href="/20ch/home/">トップ</a>
	</div>

	<h1>コメント入力画面</h1>
	<form:form modelAttribute="commentForm">
		<form:input path="comment" />
		<input type="submit">
	</form:form>
</body>
</html>